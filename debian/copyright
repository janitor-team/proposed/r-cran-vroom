This is the Debian GNU/Linux r-cran-vroom package of vroom.  The vroom
package reads and writes rectangular data quickly.  vroom was written
by Jim Hester and Hadley Wickham.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	https://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	https://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'vroom' to
'r-cran-vroom' to fit the pattern of CRAN (and non-CRAN) packages
for R.

Files: *
Copyright: 2019 - 2021  RStudio
License: MIT

Files: src/vroom_dbl.cc
Copyright: 2019 - 2021  RStudio
License: BSD

Files: src/mio/*
Copyright: 2017  https://github.com/mandreyel/
License: MIT

Files: src/grisu3.*
Copyright: 2016  Jukka Jylänki
Copyright: 2016  Mikkel F. Jørgensen
License: Apache-2

Files: debian/*
Copyright: 2021         Dirk Eddelbuettel <edd@debian.org>
License: GPL-2+

On a Debian GNU/Linux system, the GPL license (version 2) is included in the file
/usr/share/common-licenses/GPL-2, the Apache-2 license is included in the file
/usr/share/common-licenses/Apache-2.0, and the BSD license is included in the file
/usr/share/common-licenses/BSD.

The copyright statement from file src/vroom_dbl.cc reads

  An STL iterator-based string to floating point number conversion.
  This function was adapted from the C standard library of RetroBSD,
  which is based on Berkeley UNIX.
  This function and this function only is BSD license.

  https://retrobsd.googlecode.com/svn/stable/vroom_time.h|31 col 32| for
  (const autosrc str : info->column.slice(start, end)) {/libc/stdlib/strtod.c

The text of the MIT license follows:

  Permission is hereby granted, free of charge, to any person 
  obtaining a copy of this software and associated documentation 
  files (the "Software"), to deal in the Software without restriction, 
  including without limitation the rights to use, copy, modify, merge, 
  publish, distribute, sublicense, and/or sell copies of the Software, 
  and to permit persons to whom the Software is furnished to do so, 
  subject to the following conditions:
  
  The above copyright notice and this permission notice shall be 
  included in all copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS 
  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN 
  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
  IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
  THE SOFTWARE.

The file LICENSE follows:

  YEAR: 2021
  COPYRIGHT HOLDER: vroom authors

For reference, the upstream DESCRIPTION file is included below:

  Package: vroom
  Title: Read and Write Rectangular Text Data Quickly
  Version: 1.5.3
  Authors@R: 
      c(person(given = "Jim",
               family = "Hester",
               role = c("aut", "cre"),
               email = "jim.hester@rstudio.com",
               comment = c(ORCID = "0000-0002-2739-7082")),
        person(given = "Hadley",
               family = "Wickham",
               role = "aut",
               email = "hadley@rstudio.com",
               comment = c(ORCID = "0000-0003-4757-117X")),
        person(given = "https://github.com/mandreyel/",
               role = "cph",
               comment = "mio library"),
        person(given = "Jukka",
               family = "Jylänki",
               role = "cph",
               comment = "grisu3 implementation"),
        person(given = "Mikkel",
               family = "Jørgensen",
               role = "cph",
               comment = "grisu3 implementation"),
        person(given = "RStudio",
               role = c("cph", "fnd")))
  Description: The goal of 'vroom' is to read and write data (like
      'csv', 'tsv' and 'fwf') quickly. When reading it uses a quick initial
      indexing step, then reads the values lazily , so only the data you
      actually use needs to be read.  The writer formats the data in
      parallel and writes to disk asynchronously from formatting.
  License: MIT + file LICENSE
  URL: https://vroom.r-lib.org, https://github.com/r-lib/vroom
  BugReports: https://github.com/r-lib/vroom/issues
  Depends: R (>= 3.1)
  Imports: bit64, crayon, cli, glue, hms, lifecycle, methods, rlang (>=
          0.4.2), stats, tibble (>= 2.0.0), tzdb (>= 0.1.1), vctrs (>=
          0.2.0), tidyselect, withr
  Suggests: archive, bench (>= 1.1.0), covr, curl, dplyr, forcats, fs,
          ggplot2, knitr, patchwork, prettyunits, purrr, rmarkdown,
          rstudioapi, scales, spelling, testthat (>= 2.1.0), tidyr,
          waldo, xml2
  LinkingTo: progress (>= 1.2.1), cpp11 (>= 0.2.0), tzdb (>= 0.1.1)
  VignetteBuilder: knitr
  Config/testthat/edition: 3
  Config/testthat/parallel: false
  Config/Needs/website: nycflights13
  Copyright: file COPYRIGHTS
  Encoding: UTF-8
  Language: en-US
  RoxygenNote: 7.1.1
  SystemRequirements: C++11
  NeedsCompilation: yes
  Packaged: 2021-07-13 19:38:47 UTC; jhester
  Author: Jim Hester [aut, cre] (<https://orcid.org/0000-0002-2739-7082>),
    Hadley Wickham [aut] (<https://orcid.org/0000-0003-4757-117X>),
    https://github.com/mandreyel/ [cph] (mio library),
    Jukka Jylänki [cph] (grisu3 implementation),
    Mikkel Jørgensen [cph] (grisu3 implementation),
    RStudio [cph, fnd]
  Maintainer: Jim Hester <jim.hester@rstudio.com>
  Repository: CRAN
  Date/Publication: 2021-07-14 04:30:02 UTC
